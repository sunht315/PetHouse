package com.huongdt315.pethouse.presentation.component.register

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Customer
import com.huongdt315.pethouse.databinding.FragmentRegisterBinding
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {

    private val viewModel: MainViewModel by activityViewModels()

    override fun bindView() {
        with(binding) {
            tvRegisterLogin.setOnClickListener(this@RegisterFragment)
            btnRegisterRegister.setOnClickListener(this@RegisterFragment)
        }
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            viewModel.currentCustomer.collectLatest {
                handleUserState(it)
            }
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                tvRegisterLogin -> {
                    findNavController().navigate(RegisterFragmentDirections.actionGlobalLoginFragment())
                }
                btnRegisterRegister -> {
                    viewModel.registerWithNameEmailPassword(
                        edtRegisterName.text.toString(),
                        edtRegisterEmail.text.toString(),
                        edtRegisterPassword.text.toString(),
                    )
                }
                else -> {}
            }
        }
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<Customer>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToProfileEditInstructionFragment())
            }
            is ResponseStatus.Error -> {
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }
            is ResponseStatus.Loading -> {
            }
            else -> {
            }
        }
    }

}