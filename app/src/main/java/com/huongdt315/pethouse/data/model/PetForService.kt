package com.huongdt315.pethouse.data.model

data class PetForService (
    val pet: Pet,
    val services: List<Service>,
)