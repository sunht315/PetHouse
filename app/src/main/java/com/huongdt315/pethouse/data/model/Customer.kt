package com.huongdt315.pethouse.data.model

import java.util.UUID

data class Customer(
    val id: String = UUID.randomUUID().toString(),
    val name: String = "",
    val email: String = "",
    val password: String = "",
    val dateOfBirth: String = "",
    val profile: String = "",
    val background: String = "",
    val address: String = "",
    val phoneNumber: String = "",
    val pets: MutableList<Pet> = mutableListOf(),
    val bookings: MutableList<Booking> = mutableListOf(),
    val orders: MutableList<Order> = mutableListOf()
)
