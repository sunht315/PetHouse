package com.huongdt315.pethouse.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.huongdt315.pethouse.data.repo.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideCustomerRepository(
        firebaseAuth: FirebaseAuth,
        @CustomerDatabaseReferences customerDatabaseReferences: DatabaseReference,
        @CustomerProfileStorageReferences customerProfileStorageReferences: StorageReference,
        @CustomerBackgroundStorageReferences customerBackgroundStorageReferences: StorageReference,
        @PetProfileStorageReferences petProfileStorageReferences: StorageReference,
    ): CustomerRepository {
        return CustomerFirebaseRepository(
            firebaseAuth,
            customerDatabaseReferences,
            customerProfileStorageReferences,
            customerBackgroundStorageReferences,
            petProfileStorageReferences
        )
    }

    @Provides
    @Singleton
    fun providePetCenterRepository(
        @PetCenterDatabaseReferences petCenterDatabaseReferences: DatabaseReference
    ): PetCenterRepository {
        return PetCenterFirebaseRepository(petCenterDatabaseReferences)
    }

    @Provides
    @Singleton
    fun provideBookingRepository(
        @BookingDatabaseReferences petCenterDatabaseReferences: DatabaseReference
    ): BookingRepository {
        return BookingFirebaseRepository(petCenterDatabaseReferences)
    }

    @Provides
    @Singleton
    fun providePromotionRepository(
        @CustomerDatabaseReferences promotionDatabaseReferences: DatabaseReference
    ): PromotionRepository {
        return PromotionFirebaseRepository(promotionDatabaseReferences)
    }

}