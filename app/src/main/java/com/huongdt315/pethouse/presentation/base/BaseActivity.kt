package com.huongdt315.pethouse.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.viewbinding.ViewBinding
import com.huongdt315.pethouse.R

abstract class BaseActivity<VB : ViewBinding>(private val bindingInflater: (inflater: LayoutInflater) -> VB) :
    AppCompatActivity() {

    private var _binding: VB? = null
    val binding: VB
        get() = _binding!!

    lateinit var navigation: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = bindingInflater(layoutInflater)
        if (_binding == null) throw IllegalArgumentException("Binding cannot be null")
        navigation =
            (supportFragmentManager.findFragmentById(R.id.fcv_start_container) as NavHostFragment).navController

        setContentView(binding.root)
        bindView()
    }

    abstract fun bindView()

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}