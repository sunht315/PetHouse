package com.huongdt315.pethouse.data.repo

import com.huongdt315.pethouse.data.model.Booking

interface BookingRepository {

    suspend fun insertBooking(booking: Booking)

}