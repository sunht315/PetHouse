package com.huongdt315.pethouse.presentation.component.booking

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.huongdt315.pethouse.databinding.ItemSpinnerDateBinding

class DateSpinnerAdapter : BaseAdapter() {
    //    override fun bindViewHolder(binding: ItemSpinnerServiceBinding, item: BarbershopService) {
//        with(binding) {
//            val duration = String.format("%.2f minutes", item.duration)
//            tvSpinnerServiceName.text = item.name
//            tvSpinnerServiceDuration.text = duration
//
//        }
//    }
//
//    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemSpinnerServiceBinding
//        get() = { layoutInflater, viewGroup, _ ->
//            ItemSpinnerServiceBinding.inflate(layoutInflater, viewGroup, false)
//        }

    private var dates: List<String> = listOf()

    override fun getCount(): Int = dates.size

    override fun getItem(position: Int): Any = dates[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding =
            ItemSpinnerDateBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        val date = dates[position]

        with(binding) {
            tvSpinnerDate.text = date
        }
        return binding.root

    }

    fun updateList(newList: List<String>) {
        dates = newList
        notifyDataSetChanged()
    }
}