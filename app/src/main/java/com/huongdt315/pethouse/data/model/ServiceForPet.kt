package com.huongdt315.pethouse.data.model

import java.util.UUID

data class ServiceForPet(
    val id: String = UUID.randomUUID().toString(),
    val service: Service,
    val pet: Pet
)
