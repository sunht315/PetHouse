package com.huongdt315.pethouse.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.PetCenter
import com.huongdt315.pethouse.databinding.ItemPetCenterBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter

class PetCenterAdapter : BaseAdapter<ItemPetCenterBinding, PetCenter>() {
    override fun bindViewHolder(binding: ItemPetCenterBinding, item: PetCenter) {
        with(binding) {
            Glide.with(root)
                .load(item.profile)
                .centerCrop()
                .into(rivItemPetCenterProfile)

            tvItemPetCenterName.text = item.name
            tvItemPetCenterDesc.text = item.desc
            tvItemPetCenterRate.text = item.rating.toString()
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemPetCenterBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemPetCenterBinding.inflate(layoutInflater, viewGroup, false)
        }
}