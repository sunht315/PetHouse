package com.huongdt315.pethouse.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.Service
import com.huongdt315.pethouse.databinding.ItemServiceHomeBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter

class ServiceHomeAdapter: BaseAdapter<ItemServiceHomeBinding, Service>() {
    override fun bindViewHolder(binding: ItemServiceHomeBinding, item: Service) {
        with(binding) {
            Glide.with(root)
                .load(item.image)
                .centerCrop()
                .into(rivItemServiceHomeProfile)

            tvItemServiceHomeName.text = item.name
            tvItemServiceHomeDesc.text = item.desc
            tvItemServiceHomePrice.text = item.price.toString()
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemServiceHomeBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemServiceHomeBinding.inflate(layoutInflater, viewGroup, false)
        }
}