package com.huongdt315.pethouse.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class CustomerDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class BookingDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PetCenterDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PromotionDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class CustomerProfileStorageReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class CustomerBackgroundStorageReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PetProfileStorageReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ProductDatabaseReferences