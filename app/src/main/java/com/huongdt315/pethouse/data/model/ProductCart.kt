package com.huongdt315.pethouse.data.model

import java.util.UUID

data class ProductCart(
    val id: String = UUID.randomUUID().toString(),
    val product: Product? = null,
    val quantity: Int = 0
)