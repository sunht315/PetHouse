package com.huongdt315.pethouse.data.model

import java.util.UUID

data class Booking(
    val id: String = UUID.randomUUID().toString(),
    val status: String = "In process",
    val date: String = "",
    val customer: Customer,
    val petCenter: PetCenter,
    val serviceForPet: List<ServiceForPet> = listOf()
)
