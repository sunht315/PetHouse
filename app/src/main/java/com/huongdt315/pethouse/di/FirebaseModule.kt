package com.huongdt315.pethouse.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.huongdt315.pethouse.constant.FirebaseReference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object FirebaseModule {

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth = Firebase.auth

    @Provides
    @Singleton
    fun provideFirebaseDatabase(): FirebaseDatabase = FirebaseDatabase.getInstance()

    @CustomerDatabaseReferences
    @Provides
    fun provideCustomerDatabaseReferences(firebaseDatabase: FirebaseDatabase): DatabaseReference =
        firebaseDatabase.getReference(FirebaseReference.CUSTOMER)

    @PetCenterDatabaseReferences
    @Provides
    fun providePetCenterDatabaseReferences(firebaseDatabase: FirebaseDatabase): DatabaseReference =
        firebaseDatabase.getReference(FirebaseReference.PET_CENTER)

    @BookingDatabaseReferences
    @Provides
    fun provideBookingDatabaseReferences(firebaseDatabase: FirebaseDatabase): DatabaseReference =
        firebaseDatabase.getReference("bookings")

    @PromotionDatabaseReferences
    @Provides
    fun providePromotionDatabaseReferences(firebaseDatabase: FirebaseDatabase): DatabaseReference =
        firebaseDatabase.getReference(FirebaseReference.PROMOTION)

    @Provides
    @Singleton
    fun providesFirebaseStorage(): FirebaseStorage =
        FirebaseStorage.getInstance("gs://pethouse-ffbae.appspot.com")

    @CustomerProfileStorageReferences
    @Provides
    fun providesProductImageFirebaseStorageReferences(firebaseStorage: FirebaseStorage): StorageReference =
        firebaseStorage.getReference("customer_profiles/")

    @CustomerBackgroundStorageReferences
    @Provides
    fun providesServiceImageFirebaseStorageReferences(firebaseStorage: FirebaseStorage): StorageReference =
        firebaseStorage.getReference("customer_backgrounds/")

    @PetProfileStorageReferences
    @Provides
    fun providesPetProfileFirebaseStorageReferences(firebaseStorage: FirebaseStorage): StorageReference =
        firebaseStorage.getReference("pet_profiles/")

//    @Provides
//    @Singleton
//    fun provideUserFirebaseDatasource(
//        firebaseAuth: FirebaseAuth,
//        @UserDatabaseReferences databaseReferences: DatabaseReference,
//    ): UserDatasource = UserFirebaseDatasource(databaseReferences, firebaseAuth)
}