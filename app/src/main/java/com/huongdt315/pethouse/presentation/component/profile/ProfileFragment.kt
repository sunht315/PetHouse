package com.huongdt315.pethouse.presentation.component.profile

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.databinding.FragmentProfileBinding
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment: BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    @Inject
    lateinit var petAdapter: PetAdapter

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModels() {
        lifecycleScope.launch {
            mainViewModel.currentCustomer.collectLatest {
                if(it is ResponseStatus.Success) {
                    with(binding) {
                        Glide.with(root)
                            .load(it.data!!.profile)
                            .into(civProfileProfile)

                        Glide.with(root)
                            .load(it.data!!.background)
                            .into(rivProfileBackground)

                        tvProfileName.text = it.data!!.name
                        tvProfileStatus.text = "My pets are best"
                        tvProfileAddress.text = it.data!!.address
                        tvProfileDateOfBirth.text = it.data!!.dateOfBirth

                        rcvProfilePetProfiles.run {
                            adapter = petAdapter.apply {
                                updateList(it.data!!.pets)
                            }
                            layoutManager = LinearLayoutManager(requireContext())
                        }

                        tvProfilePetProfiles.text = "My family has ${it.data!!.pets.size} members"
                    }
                }
            }
        }
    }

    override fun bindView() {
        mainViewModel.getCurrentUser()
    }
}