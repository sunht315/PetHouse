package com.huongdt315.pethouse.presentation

import android.view.View
import androidx.activity.viewModels
import androidx.navigation.ui.setupWithNavController
import com.huongdt315.pethouse.R
import com.huongdt315.pethouse.databinding.ActivityMainBinding
import com.huongdt315.pethouse.presentation.base.BaseActivity
import com.huongdt315.pethouse.presentation.component.PetCenterViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {

    private val petCenterViewModel: PetCenterViewModel by viewModels()
    private val mainViewModel: MainViewModel by viewModels()

    override fun bindView() {
        initData()
        navigation.addOnDestinationChangedListener { _, destination, _ ->
            when(destination.id) {
                R.id.homeFragment -> showBottomNavigation()
                R.id.profileFragment -> showBottomNavigation()
                R.id.searchFragment -> showBottomNavigation()
                R.id.shopFragment -> showBottomNavigation()
                else -> hideBottomNavigation()
            }
        }
        binding.bnvMainNavigation.setupWithNavController(navigation)

    }

    private fun initData() {

    }
    private fun showBottomNavigation() {
        binding.bnvMainNavigation.visibility = View.VISIBLE
    }

    private fun hideBottomNavigation() {
        binding.bnvMainNavigation.visibility = View.GONE
    }

    fun showLoading() {
        binding.llcLoading.visibility = View.VISIBLE
        binding.root.isEnabled = false

    }

    fun hideLoading() {
        binding.llcLoading.visibility = View.GONE
        binding.root.isEnabled = true
    }

}