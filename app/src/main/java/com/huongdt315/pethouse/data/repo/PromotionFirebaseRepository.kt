package com.huongdt315.pethouse.data.repo

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.huongdt315.pethouse.data.model.PetCenter
import com.huongdt315.pethouse.data.model.Promotion
import com.huongdt315.pethouse.di.PromotionDatabaseReferences
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

class PromotionFirebaseRepository @Inject constructor(
    @PromotionDatabaseReferences private val promotionDatabaseReferences: DatabaseReference
): PromotionRepository {
    override suspend fun getPromotions(): Flow<List<Promotion>> {
        val promotions = mutableListOf<Promotion>(
            Promotion(promotion = "https://img.freepik.com/premium-psd/pet-store-social-media-promotion-instagram-banner-post-design-template_205739-337.jpg?w=360"),
            Promotion(promotion = "https://img.freepik.com/premium-psd/pet-store-social-media-promotion-instagram-banner-post-design-template_205739-415.jpg?w=2000"),
            Promotion(promotion = "https://www.printglobe.com/blog/wp-content/uploads/pet-promotions-4.jpg"),
        )

        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    promotions.clear()
                    for (promotion in snapshot.children) {
                        promotion.getValue(Promotion::class.java)
                            ?.let { promotions.add(it) }
                    }
                    trySend(promotions)
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }

            promotionDatabaseReferences.addValueEventListener(valueEventListener)
            awaitClose { promotionDatabaseReferences.removeEventListener(valueEventListener) }
        }
    }
}