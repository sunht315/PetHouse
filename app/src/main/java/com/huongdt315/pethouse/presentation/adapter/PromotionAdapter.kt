package com.huongdt315.pethouse.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.Promotion
import com.huongdt315.pethouse.databinding.ItemPromotionBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter

class PromotionAdapter : BaseAdapter<ItemPromotionBinding, Promotion>() {
    override fun bindViewHolder(binding: ItemPromotionBinding, item: Promotion) {
        with(binding) {
            Glide.with(root)
                .load(item.promotion)
                .centerCrop()
                .into(rivItemPromotionPromotion)
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemPromotionBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemPromotionBinding.inflate(layoutInflater, viewGroup, false)
        }
}