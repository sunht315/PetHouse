package com.huongdt315.pethouse.presentation.component.booking

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.PetForService
import com.huongdt315.pethouse.databinding.ItemPetForServiceBookingBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter

class PetForServiceAdapter : BaseAdapter<ItemPetForServiceBookingBinding, PetForService>() {
    override fun bindViewHolder(binding: ItemPetForServiceBookingBinding, item: PetForService) {
        binding.tvItemServiceForPetName.text = item.pet.name
        binding.tvItemServiceForPetWeight.text = "${item.pet.weight}kg"
        Glide.with(binding.root).load(item.pet.profile).into(binding.civItemPetForServiceBooking)

        binding.rcvService.run {
            adapter = ServiceForPetBookingAdapter().apply {
                updateList(item.services)
            }
            layoutManager = LinearLayoutManager(this.context)
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemPetForServiceBookingBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemPetForServiceBookingBinding.inflate(layoutInflater, viewGroup, false)
        }
}