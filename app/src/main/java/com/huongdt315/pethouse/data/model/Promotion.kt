package com.huongdt315.pethouse.data.model

import java.util.UUID

data class Promotion(
    val id: String = UUID.randomUUID().toString(),
    val promotion: String = ""
)