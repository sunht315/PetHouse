package com.huongdt315.pethouse.data.model

import com.google.firebase.database.Exclude
import java.util.*

data class PetCenter(
    val id: String = UUID.randomUUID().toString(),
    val name: String = "",
    var profile: String = "",
    val email: String = "",
    val address: String = "",
    val desc: String = "",
    val hotline: String = "",
    val rating: Double = 5.0,

    @Exclude
    @get:Exclude
    @set:Exclude
    var services: MutableList<Service> = mutableListOf(),

    @Exclude
    @get:Exclude
    @set:Exclude
    var products: MutableList<Product> = mutableListOf()


) {
    override fun equals(other: Any?): Boolean {
        return false
    }
}