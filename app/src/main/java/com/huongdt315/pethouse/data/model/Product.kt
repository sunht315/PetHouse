package com.huongdt315.pethouse.data.model

import java.util.UUID

data class Product(
    var id: String = UUID.randomUUID().toString(),
    val name: String = "",
    var image: String = "",
    val desc: String = "",
    val type: String = "",
    val price: Double = 0.0,
    val rating: Double = 0.0,
)
