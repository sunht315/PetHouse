package com.huongdt315.pethouse.presentation.component.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.Pet
import com.huongdt315.pethouse.databinding.ItemPetProfileBinding
import com.huongdt315.pethouse.databinding.ItemServiceBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter

class PetAdapter : BaseAdapter<ItemPetProfileBinding, Pet>() {
    override fun bindViewHolder(binding: ItemPetProfileBinding, item: Pet) {
        with(binding) {
            Glide.with(root)
                .load(item.profile)
                .centerCrop()
                .into(civItemPetProfile)

            tvItemPetProfileName.text = item.name
            tvItemPetProfileSpecies.text = item.species
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemPetProfileBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemPetProfileBinding.inflate(layoutInflater, viewGroup, false)
        }
}