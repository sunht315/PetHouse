package com.huongdt315.pethouse.presentation.component.service

import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.ServiceForPet
import com.huongdt315.pethouse.databinding.FragmentServiceDetailBinding
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.base.BaseFragment
import com.huongdt315.pethouse.presentation.component.PetCenterViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import javax.inject.Inject

@AndroidEntryPoint
class ServiceDetailFragment :
    BaseFragment<FragmentServiceDetailBinding>(FragmentServiceDetailBinding::inflate) {

    private val args: ServiceDetailFragmentArgs by navArgs()

    @Inject
    lateinit var serviceForPetAdapter: ServiceForPetAdapter

    private val petCenterViewModel: PetCenterViewModel by activityViewModels()
    private val mainViewModel: MainViewModel by activityViewModels()

    override fun bindView() {
        mainViewModel.getCurrentUser()

        lifecycleScope.launch {

            val service = petCenterViewModel.getServiceDetail(args.serviceId, args.petCenterId)
            val pets = mainViewModel.currentCustomer.value?.data?.pets

            with(binding) {
                tvServiceDetailName.text = service?.name
                tvServiceDetailDesc.text = service?.desc
                tvServiceDetailPrice.text = "${DecimalFormat("#,###").format(service?.price)}đ"
                Glide.with(root)
                    .load(service?.image)
                    .centerCrop()
                    .into(rivServiceDetailBanner)

                rcvServiceDetailPets.run {
                    adapter = serviceForPetAdapter.apply {
                        if (pets != null) {
                            setList(pets)
                        }
                    }
                    layoutManager =
                        LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                }
            }

            launch {
                serviceForPetAdapter.selectedPetNum.collectLatest {
                    binding.tvServiceDetailTotalPrice.text =
                        "${DecimalFormat("#,###").format(service!!.price * it)}đ"
                }
            }
            launch {
                serviceForPetAdapter.selectedPet.observe(viewLifecycleOwner) {
                    val serviceForPets = it.map { pet ->
                        ServiceForPet(pet = pet, service = service!!)
                    }

                    petCenterViewModel.serviceForPets = serviceForPets as MutableList<ServiceForPet>
                }
            }

            binding.btnServiceDetailBookNow.setOnClickListener {
                findNavController().navigate(ServiceDetailFragmentDirections.actionServiceDetailFragmentToFragmentBooking(args.petCenterId))
            }


        }

    }
}