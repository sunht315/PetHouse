package com.huongdt315.pethouse.constant

object FirebaseReference {
    const val CUSTOMER = "customers"
    const val PET_CENTER = "pet_centers"
    const val PROMOTION = "promotions"
}