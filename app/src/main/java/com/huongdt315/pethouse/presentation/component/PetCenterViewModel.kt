package com.huongdt315.pethouse.presentation.component

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.huongdt315.pethouse.data.model.Booking
import com.huongdt315.pethouse.data.model.Customer
import com.huongdt315.pethouse.data.model.PetCenter
import com.huongdt315.pethouse.data.model.Product
import com.huongdt315.pethouse.data.model.Promotion
import com.huongdt315.pethouse.data.model.Service
import com.huongdt315.pethouse.data.model.ServiceForPet
import com.huongdt315.pethouse.data.repo.BookingRepository
import com.huongdt315.pethouse.data.repo.PetCenterRepository
import com.huongdt315.pethouse.data.repo.PromotionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class PetCenterViewModel @Inject constructor(
    private val petCenterRepository: PetCenterRepository,
    private val bookingRepository: BookingRepository
) : ViewModel() {

    val dates by lazy {
        var currentDate = System.currentTimeMillis()

        val dates = mutableListOf<String>()
        repeat(7) {
            val dateString = SimpleDateFormat("EEE, MMM dd", Locale.US).format(
                currentDate
            )
            dates.add(dateString)
            currentDate += 24 * 60 * 60 * 1000
        }
        dates
    }

    var serviceForPets: MutableList<ServiceForPet> = mutableListOf()
    var petCenterSelected: PetCenter? = null

    private var _petCenters =
        MutableStateFlow<List<PetCenter>>(listOf())
    val petCenters get() = _petCenters.asStateFlow()

    private var _selectedPetCenter =
        MutableStateFlow<PetCenter?>(null)
    val selectedPetCenter get() = _selectedPetCenter.asStateFlow()

    private var _services =
        MutableStateFlow<List<Service>>(listOf())
    val services get() = _services.asStateFlow()

    private var _selectedServices =
        MutableStateFlow<List<Service>>(listOf())
    val selectedServices get() = _selectedServices.asStateFlow()


    private var _products =
        MutableStateFlow<List<Product>>(listOf())
    val products get() = _products.asStateFlow()

    private var _selectedProducts =
        MutableStateFlow<List<Product>>(listOf())
    val selectedProducts get() = _selectedProducts.asStateFlow()

    @OptIn(FlowPreview::class)
    fun getPetCenters() {
        viewModelScope.launch {
            launch {
                petCenterRepository.getPetCentersInformation().flatMapConcat {
                    _petCenters.value = it
                    petCenterRepository.getServicesByPetCenterId(it[0].id)
                }.collect {
                    if (_services.value.isNotEmpty()) {
                        val list = _services.value as MutableList
                        _services.value = list.apply { addAll(it) }
                    } else {
                        _services.value = it as MutableList
                    }

                    Log.d("ducna", "getPetCenters: ${_services.value}")
                }
            }
            launch {
                petCenterRepository.getPetCentersInformation().flatMapConcat {
                    _petCenters.value = it
                    petCenterRepository.getServicesByPetCenterId(it[1].id)
                }.collect {
                    if (_services.value.isNotEmpty()) {
                        val list = _services.value as MutableList
                        _services.value = list.apply { addAll(it) }
                    } else {
                        _services.value = it as MutableList
                    }

                    Log.d("ducna", "getPetCenters: ${_services.value}")
                }
            }
            launch {
                petCenterRepository.getPetCentersInformation().flatMapConcat {
                    _petCenters.value = it
                    petCenterRepository.getServicesByPetCenterId(it[2].id)
                }.collect {
                    if (_services.value.isNotEmpty()) {
                        val list = _services.value as MutableList
                        _services.value = list.apply { addAll(it) }
                    } else {
                        _services.value = it as MutableList
                    }

                    Log.d("ducna", "getPetCenters: ${_services.value}")
                }
            }
            launch {
                petCenterRepository.getPetCentersInformation().flatMapConcat {
                    _petCenters.value = it
                    petCenterRepository.getProductsByPetCenterId(it[0].id)
                }.collect {
                    if (_products.value.isNotEmpty()) {
                        val list = _products.value as MutableList
                        _products.value = list.apply { addAll(it) }
                    } else {
                        _products.value = it as MutableList
                    }

                    Log.d("ducna", "getPetCenters: ${_products.value}")
                }
            }
            launch {
                petCenterRepository.getPetCentersInformation().flatMapConcat {
                    _petCenters.value = it
                    petCenterRepository.getProductsByPetCenterId(it[1].id)
                }.collect {
                    if (_products.value.isNotEmpty()) {
                        val list = _products.value as MutableList
                        _products.value = list.apply { addAll(it) }
                    } else {
                        _products.value = it as MutableList
                    }

                    Log.d("ducna", "getPetCenters: ${_products.value}")
                }
            }
            launch {
                petCenterRepository.getPetCentersInformation().flatMapConcat {
                    _petCenters.value = it
                    petCenterRepository.getProductsByPetCenterId(it[2].id)
                }.collect {
                    if (_products.value.isNotEmpty()) {
                        val list = _products.value as MutableList
                        _products.value = list.apply { addAll(it) }
                    } else {
                        _products.value = it as MutableList
                    }

                    Log.d("ducna", "getPetCenters: ${_products.value}")
                }
            }
        }
    }

    @OptIn(FlowPreview::class)
    fun getPetCenterById(petCenterId: String) {
        viewModelScope.launch {
            petCenterRepository.getPetCenterById(petCenterId).flatMapConcat {
                _selectedPetCenter.value = it
                petCenterRepository.getServicesByPetCenterId(it!!.id)
            }.collect {
                _selectedServices.value = it as MutableList<Service>
            }
        }
    }

    suspend fun getServiceDetail(serviceId: String, petCenterId: String) =
        petCenterRepository.getServiceById(petCenterId, serviceId)

    fun insertBooking(
        date: String,
        customer: Customer,
        petCenter: PetCenter,
        serviceForPet: List<ServiceForPet>
    ) {
        viewModelScope.launch {
            bookingRepository.insertBooking(
                Booking(
                    date = date,
                    customer = customer,
                    petCenter = petCenter,
                    serviceForPet = serviceForPet
                )
            )
        }
    }

}