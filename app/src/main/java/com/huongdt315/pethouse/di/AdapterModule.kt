package com.huongdt315.pethouse.di

import com.huongdt315.pethouse.presentation.adapter.PetCenterAdapter
import com.huongdt315.pethouse.presentation.adapter.PromotionAdapter
import com.huongdt315.pethouse.presentation.adapter.ServiceAdapter
import com.huongdt315.pethouse.presentation.adapter.ServiceHomeAdapter
import com.huongdt315.pethouse.presentation.component.booking.PetForServiceAdapter
import com.huongdt315.pethouse.presentation.component.profile.PetAdapter
import com.huongdt315.pethouse.presentation.component.service.ServiceForPetAdapter
import com.huongdt315.pethouse.presentation.component.shop.ProductAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent


@Module
@InstallIn(FragmentComponent::class)
object AdapterModule {

    @Provides
    fun providesPetCenterAdapter(): PetCenterAdapter = PetCenterAdapter()

    @Provides
    fun providesServiceAdapter(): ServiceAdapter = ServiceAdapter()

    @Provides
    fun providesServiceHomeAdapter(): ServiceHomeAdapter = ServiceHomeAdapter()

    @Provides
    fun providesProductAdapter(): ProductAdapter = ProductAdapter()

    @Provides
    fun providesPromotionAdapter(): PromotionAdapter = PromotionAdapter()

    @Provides
    fun providesServiceForPetAdapter(): ServiceForPetAdapter = ServiceForPetAdapter()

    @Provides
    fun providesPetAdapter(): PetAdapter = PetAdapter()

    @Provides
    fun providesPetForServiceAdapter(): PetForServiceAdapter = PetForServiceAdapter()


}