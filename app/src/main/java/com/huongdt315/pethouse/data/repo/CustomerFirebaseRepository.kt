package com.huongdt315.pethouse.data.repo

import android.net.Uri
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.StorageReference
import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Customer
import com.huongdt315.pethouse.data.model.Pet
import com.huongdt315.pethouse.di.CustomerBackgroundStorageReferences
import com.huongdt315.pethouse.di.CustomerDatabaseReferences
import com.huongdt315.pethouse.di.CustomerProfileStorageReferences
import com.huongdt315.pethouse.di.PetProfileStorageReferences
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class CustomerFirebaseRepository @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    @CustomerDatabaseReferences private val customerDatabaseReferences: DatabaseReference,
    @CustomerProfileStorageReferences private val customerProfileStorageReferences: StorageReference,
    @CustomerBackgroundStorageReferences private val customerBackgroundStorageReferences: StorageReference,
    @PetProfileStorageReferences private val petProfileStorageReferences: StorageReference,
) : CustomerRepository {

    override suspend fun insertCustomer(
        name: String,
        email: String,
        password: String
    ): ResponseStatus<Customer> {
        return try {
            firebaseAuth.createUserWithEmailAndPassword(
                email,
                password
            ).await()

            if (firebaseAuth.currentUser != null) {
                val registeredUser =
                    Customer(
                        id = firebaseAuth.currentUser!!.uid,
                        email = email,
                        password = password,
                        name = name
                    )
                customerDatabaseReferences.child(registeredUser.id).setValue(registeredUser).await()
                ResponseStatus.Success(registeredUser)
            } else {
                ResponseStatus.Error("Something when wrong")
            }
        } catch (exception: FirebaseException) {
            ResponseStatus.Error(exception.message!!)
        }
    }

    override suspend fun getCustomerByEmailAndPassword(
        email: String,
        password: String
    ): ResponseStatus<Customer> {
        return try {

            firebaseAuth.signInWithEmailAndPassword(email, password).await()

            val customer = customerDatabaseReferences.child(firebaseAuth.currentUser!!.uid).get()
                .await().getValue(Customer::class.java)

            ResponseStatus.Success(customer!!)
        } catch (exception: FirebaseException) {
            ResponseStatus.Error(exception.message!!)
        }
    }

    override suspend fun updateCustomerInformation(
        dateOfBirth: String,
        address: String,
        describe: String,
        profile: String,
        background: String,
        pets: List<Pet>
    ): Boolean {
        if (firebaseAuth.currentUser == null) return false

        val currentCustomerReference = firebaseAuth.currentUser?.uid?.let {
            customerDatabaseReferences.child(it)
        }

        updateCustomerProfile(profile).combine(updateCustomerBackground(background)) { p, b ->
            {
                mapOf(
                    "dateOfBirth" to dateOfBirth,
                    "address" to address,
                    "describe" to describe,
                    "profile" to p,
                    "background" to b,
                    "pets" to pets
                )

            }
        }.collectLatest {
            currentCustomerReference?.updateChildren(it())
        }

        updatePetProfile(pets[0].id, pets[0].profile).collectLatest {
            val map = mapOf(
                "profile" to pets[0].profile
            )

            currentCustomerReference?.child("0")?.updateChildren(map)
        }

        return true
    }

    override suspend fun updateCustomerProfile(profile: String): Flow<String> = callbackFlow {
        customerProfileStorageReferences.child(firebaseAuth.currentUser!!.uid).run {
            putFile(Uri.parse(profile)).await()
            downloadUrl.addOnSuccessListener {
                trySend(it.toString())
            }
        }

        awaitClose {
            customerProfileStorageReferences.child(firebaseAuth.currentUser!!.uid).downloadUrl.addOnCanceledListener { }
        }
    }

    override suspend fun updateCustomerBackground(background: String): Flow<String> = callbackFlow {
        customerBackgroundStorageReferences.child(firebaseAuth.currentUser!!.uid).run {
            putFile(Uri.parse(background)).await()
            downloadUrl.addOnSuccessListener {
                trySend(it.toString())
            }
        }

        awaitClose {
            customerBackgroundStorageReferences.child(firebaseAuth.currentUser!!.uid).downloadUrl.addOnCanceledListener { }
        }
    }

    override suspend fun updatePetProfile(petId: String, profile: String): Flow<String> =
        callbackFlow {
            petProfileStorageReferences.child(petId).run {
                putFile(Uri.parse(profile)).await()
                downloadUrl.addOnSuccessListener {
                    trySend(it.toString())
                }
            }

            awaitClose {
                petProfileStorageReferences.child(firebaseAuth.currentUser!!.uid).downloadUrl.addOnCanceledListener { }
            }
        }


    override suspend fun getCurrentCustomer(): Flow<ResponseStatus<Customer>> = callbackFlow {
        if (firebaseAuth.currentUser == null) {
            trySend(ResponseStatus.Error("Null user"))
        }

        val currentUserReference = firebaseAuth.currentUser?.uid?.let {
            customerDatabaseReferences.child(
                it
            )
        }

        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val userLogin = dataSnapshot.getValue(Customer::class.java)

                trySend(
                    if (userLogin != null)
                        ResponseStatus.Success(userLogin)
                    else ResponseStatus.Error("Not found user")
                )
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }

        currentUserReference?.addValueEventListener(listener)

        awaitClose {
            currentUserReference?.removeEventListener(listener)
        }
    }
}