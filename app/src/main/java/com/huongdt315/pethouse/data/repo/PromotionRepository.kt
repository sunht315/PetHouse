package com.huongdt315.pethouse.data.repo

import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Promotion
import kotlinx.coroutines.flow.Flow

interface PromotionRepository {

    suspend fun getPromotions(): Flow<List<Promotion>>

}