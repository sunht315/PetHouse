package com.huongdt315.pethouse.data.repo

import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Customer
import com.huongdt315.pethouse.data.model.Pet
import kotlinx.coroutines.flow.Flow

interface CustomerRepository {

    suspend fun insertCustomer(name: String, email: String, password: String): ResponseStatus<Customer>

    suspend fun getCustomerByEmailAndPassword(
        email: String,
        password: String
    ): ResponseStatus<Customer>

    suspend fun updateCustomerInformation(
        dateOfBirth: String,
        address: String,
        describe: String,
        profile: String,
        background: String,
        pets: List<Pet>
    ): Boolean

    suspend fun updateCustomerProfile(
        profile: String,
    ): Flow<String>

    suspend fun updateCustomerBackground(
        background: String,
    ): Flow<String>

    suspend fun updatePetProfile(
        petId: String,
        profile: String,
    ): Flow<String>

    suspend fun getCurrentCustomer(): Flow<ResponseStatus<Customer>>

}