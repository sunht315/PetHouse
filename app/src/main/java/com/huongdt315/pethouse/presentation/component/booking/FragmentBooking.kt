package com.huongdt315.pethouse.presentation.component.booking

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.Pet
import com.huongdt315.pethouse.data.model.PetForService
import com.huongdt315.pethouse.data.model.Service
import com.huongdt315.pethouse.data.model.ServiceForPet
import com.huongdt315.pethouse.databinding.FragmentBookingBinding
import com.huongdt315.pethouse.presentation.base.BaseFragment
import com.huongdt315.pethouse.presentation.component.PetCenterViewModel
import com.huongdt315.pethouse.presentation.component.service.ServiceDetailFragmentArgs
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import javax.inject.Inject

@AndroidEntryPoint
class FragmentBooking : BaseFragment<FragmentBookingBinding>(FragmentBookingBinding::inflate) {

    private val args: FragmentBookingArgs by navArgs()

    @Inject
    lateinit var petForServiceAdapter: PetForServiceAdapter

    private val petCenterViewModel: PetCenterViewModel by activityViewModels()

    override fun bindView() {
        petCenterViewModel.getPetCenterById(args.petCenterId)

        lifecycleScope.launch {
            petCenterViewModel.selectedPetCenter.collectLatest {
                it?.let {
                    binding.tvBookingPetCenterName.text = it.name
                    binding.tvBookingPetCenterAddress.text = it.address
                    Glide.with(binding.root).load(it.profile)
                        .into(binding.ivBookingPetCenterProfile)
                }
            }
        }

        val serviceForPets = petCenterViewModel.serviceForPets

        val map = getServicesByPet(serviceForPets)
        val petForServices = mutableListOf<PetForService>()
        serviceForPets.forEach { serviceForPet ->
            map[serviceForPet.pet]?.let {
                petForServices.add(PetForService(serviceForPet.pet, it))
            }
        }

        binding.rcvBookingPet.run {
            adapter = petForServiceAdapter.apply {
                updateList(petForServices)
            }
            layoutManager = LinearLayoutManager(requireContext())
        }

        var totalPrice = 0.0

        serviceForPets.forEach {
            totalPrice += it.service.price
        }

        binding.tvBookingTotalPrice.text = "${DecimalFormat("#,###").format(totalPrice)}đ"
        binding.tvBookingProvisionalPrice.text = "${DecimalFormat("#,###").format(totalPrice)}đ"


        binding.spnBookingTime.run {
            adapter = DateSpinnerAdapter().apply {
                updateList(petCenterViewModel.dates)
            }
        }

//        binding.btnBookingBook.setOnClickListener {
//            petCenterViewModel.insertBooking(
//                petCenterViewModel.dates[0],
//
//            )
//        }
    }

    private fun getServicesByPet(servicesForPets: List<ServiceForPet>): Map<Pet, List<Service>> {
        return servicesForPets.groupBy(ServiceForPet::pet, ServiceForPet::service)
    }

}