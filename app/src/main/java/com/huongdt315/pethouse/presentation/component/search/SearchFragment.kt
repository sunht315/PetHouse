package com.huongdt315.pethouse.presentation.component.search

import com.huongdt315.pethouse.databinding.FragmentSearchBinding
import com.huongdt315.pethouse.presentation.base.BaseFragment

class SearchFragment : BaseFragment<FragmentSearchBinding>(FragmentSearchBinding::inflate) {
}