package com.huongdt315.pethouse.data.repo

import com.google.firebase.database.DatabaseReference
import com.huongdt315.pethouse.data.model.Booking
import com.huongdt315.pethouse.di.BookingDatabaseReferences
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class BookingFirebaseRepository @Inject constructor(
    @BookingDatabaseReferences private val bookingDatabaseReferences: DatabaseReference
): BookingRepository {

    override suspend fun insertBooking(booking: Booking) {
        bookingDatabaseReferences.child(booking.id).setValue(booking).await()
    }

}