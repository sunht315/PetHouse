package com.huongdt315.pethouse.presentation.component.petcenter

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.huongdt315.pethouse.databinding.FragmentPetCenterBinding
import com.huongdt315.pethouse.presentation.adapter.ServiceAdapter
import com.huongdt315.pethouse.presentation.base.BaseFragment
import com.huongdt315.pethouse.presentation.component.PetCenterViewModel
import com.huongdt315.pethouse.presentation.component.home.HomeFragmentDirections
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PetCenterFragment :
    BaseFragment<FragmentPetCenterBinding>(FragmentPetCenterBinding::inflate) {

    private val args: PetCenterFragmentArgs by navArgs()

    private val petCenterViewModel: PetCenterViewModel by viewModels()

    @Inject
    lateinit var serviceAdapter: ServiceAdapter

    override fun observeViewModels() {
        lifecycleScope.launch {
            launch {
                petCenterViewModel.selectedPetCenter.collectLatest {
                    it?.let { petCenter ->
                        petCenterViewModel.petCenterSelected = petCenter
                        with(binding) {
                            tvPetCenterName.text = petCenter.name
                            tvPetCenterHotline.text = "Hotline: ${petCenter.hotline}"
                            tvPetCenterAddress.text = "Address: ${petCenter.address}"
                        }
//                    Toast.makeText(requireContext(), petCenter.services.size, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            launch {
                petCenterViewModel.selectedServices.collectLatest {
                    serviceAdapter.apply {
                        if (it.isNotEmpty()) {
                            updateList(it)
                            Log.d("ducna", "observeViewModels: $it")
                            setOnItemClickListener {
                                findNavController().navigate(PetCenterFragmentDirections.actionPetCenterFragmentToServiceFragment(args.petCenterId, it.id))
                            }
                        }
                    }
                    binding.rcvPetCenterService.run {
                        layoutManager =
                            LinearLayoutManager(
                                requireContext(),
                            )
                        adapter = serviceAdapter
                    }
                }
            }

        }
    }

    override fun bindView() {
        petCenterViewModel.getPetCenterById(args.petCenterId)
    }
}