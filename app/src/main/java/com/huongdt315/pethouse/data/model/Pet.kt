package com.huongdt315.pethouse.data.model

import java.util.*

data class Pet(
    val id: String = UUID.randomUUID().toString(),
    val name: String = "",
    val profile: String = "",
    val species: String = "",
    val dateOfBirth: String = "",
    val gender: String = "",
    val weight: Double = 0.0,
    val desc: String = "",
    val allergy: String = "",
    val deworming: String = "",
    val vaccination: String = ""
)
