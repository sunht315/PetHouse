package com.huongdt315.pethouse.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.Service
import com.huongdt315.pethouse.databinding.ItemServiceBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter
import java.text.DecimalFormat


class ServiceAdapter: BaseAdapter<ItemServiceBinding, Service>() {
    override fun bindViewHolder(binding: ItemServiceBinding, item: Service) {
        with(binding) {
            Glide.with(root)
                .load(item.image)
                .centerCrop()
                .into(rivItemServiceProfile)

            tvItemServiceName.text = item.name
            tvItemServiceDesc.text = item.desc
            tvItemServicePrice.text = "${DecimalFormat("#,###").format(item.price)}đ"
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemServiceBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemServiceBinding.inflate(layoutInflater, viewGroup, false)
        }
}