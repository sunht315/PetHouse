package com.huongdt315.pethouse.presentation.component.service

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.R
import com.huongdt315.pethouse.data.model.Pet
import com.huongdt315.pethouse.databinding.ItemServiceForPetBinding
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update

class ServiceForPetAdapter : RecyclerView.Adapter<ServiceForPetAdapter.ServiceForPetHolder>() {

    private var onItemClickListener: ((item: Pet) -> Unit)? = null

    val selectedPet = MutableLiveData<MutableList<Pet>>(mutableListOf())
    val selectedPetNum = MutableStateFlow<Int>(0)

    private var pets = listOf<Pet>()

    fun setList(t: List<Pet>) {
        pets = t
    }

    inner class ServiceForPetHolder(val binding: ItemServiceForPetBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private var isSelected = false

        fun bind(pet: Pet) {

            Glide.with(binding.root)
                .load(pet.profile)
                .into(binding.civItemServiceForPet)

            binding.tvItemServiceForPetName.text = pet.name
            binding.tvItemServiceForPetWeight.text = "${pet.weight}kg"
        }

        fun toggleSelect(pet: Pet) {
            if (!isSelected) {
                binding.root.background = AppCompatResources.getDrawable(
                    binding.root.context,
                    R.drawable.bg_rounded_blue_solid
                )
                val list = mutableListOf<Pet>()
                selectedPet.value?.apply { add(pet) }?.let { list.addAll(it) }
                selectedPet.value = list
                selectedPetNum.value += 1
            } else {
                binding.root.background = AppCompatResources.getDrawable(
                    binding.root.context,
                    R.drawable.bg_rounded_gray_solid
                )
                val list = mutableListOf<Pet>()
                selectedPet.value?.apply { remove(pet) }?.let { list.addAll(it) }
                selectedPet.value = list
                selectedPetNum.value -= 1

            }
            isSelected = !isSelected
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceForPetHolder {
        val binding =
            ItemServiceForPetBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ServiceForPetHolder(binding)
    }

    override fun onBindViewHolder(holder: ServiceForPetHolder, position: Int) {
        val time = pets[position]

        holder.bind(time)
        holder.binding.root.setOnClickListener {
            holder.toggleSelect(time)
            onItemClickListener?.let { it1 -> it1(time) }
        }
    }

    override fun getItemCount(): Int = pets.size

    fun setOnItemClickListener(listener: (item: Pet) -> Unit) {
        onItemClickListener = listener
    }
}