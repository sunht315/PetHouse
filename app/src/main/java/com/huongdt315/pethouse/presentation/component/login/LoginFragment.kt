package com.huongdt315.pethouse.presentation.component.login

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.huongdt315.pethouse.R
import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Customer
import com.huongdt315.pethouse.databinding.FragmentLoginBinding
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.base.BaseFragment
import com.huongdt315.pethouse.util.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    private val viewModel: MainViewModel by activityViewModels()

    override fun bindView() {
        with(binding) {
            tvLoginCreateAccount.setOnClickListener(this@LoginFragment)
            btnLoginLogin.setOnClickListener(this@LoginFragment)
        }
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.currentCustomer.collectLatest {
                    handleUserState(it)
                }
            }

        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                tvLoginCreateAccount -> {
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
                }
                btnLoginLogin -> {
                    viewModel.login(
                        edtLoginEmail.text.toString(),
                        edtLoginPassword.text.toString()
                    )
                }
            }
        }
    }

    private fun handleUserState(userResponseStatus: ResponseStatus<Customer>?) {
        when (userResponseStatus) {
            is ResponseStatus.Success -> {
                findNavController().navigate(R.id.homeFragment)
            }
            is ResponseStatus.Error -> {
                Toast.makeText(requireContext(), userResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }
            is ResponseStatus.Loading -> {
            }
            else -> {
            }
        }
    }
}