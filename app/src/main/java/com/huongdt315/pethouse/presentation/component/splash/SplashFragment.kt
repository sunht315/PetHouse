package com.huongdt315.pethouse.presentation.component.splash

import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Customer
import com.huongdt315.pethouse.databinding.FragmentSplashBinding
import com.huongdt315.pethouse.presentation.MainActivity
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun bindView() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(1000)
            findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
        }
    }

    private fun handleCustomerResponse(customerResponseStatus: ResponseStatus<Customer>?) {
        when (customerResponseStatus) {
            is ResponseStatus.Success -> {
                (requireActivity() as MainActivity).hideLoading()
            }
            is ResponseStatus.Error -> {
                (requireActivity() as MainActivity).hideLoading()
                Toast.makeText(requireContext(), customerResponseStatus.msg, Toast.LENGTH_SHORT).show()
            }
            is ResponseStatus.Loading -> {
                (requireActivity() as MainActivity).showLoading()
            }
            else -> {
                (requireActivity() as MainActivity).hideLoading()
                findNavController().navigate(SplashFragmentDirections.actionGlobalLoginFragment())
            }
        }
    }
}