package com.huongdt315.pethouse.presentation.component.booking

import android.view.LayoutInflater
import android.view.ViewGroup
import com.huongdt315.pethouse.data.model.Service
import com.huongdt315.pethouse.databinding.ItemPetForServiceBookingBinding
import com.huongdt315.pethouse.databinding.ItemServiceForPetBookingBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter
import java.text.DecimalFormat


class ServiceForPetBookingAdapter: BaseAdapter<ItemServiceForPetBookingBinding, Service>() {
    override fun bindViewHolder(binding: ItemServiceForPetBookingBinding, item: Service) {
        binding.tvServiceForPetBookingName.text = item.name
        binding.tvServiceForPetBookingPrice.text = "${DecimalFormat("#,###").format(item.price)}đ"
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemServiceForPetBookingBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemServiceForPetBookingBinding.inflate(layoutInflater, viewGroup, false)
        }
}