package com.huongdt315.pethouse.presentation.component.profile

import android.net.Uri
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.huongdt315.pethouse.databinding.FragmentProfileEditBinding
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ProfileEditFragment :
    BaseFragment<FragmentProfileEditBinding>(FragmentProfileEditBinding::inflate) {


    @Inject
    lateinit var petAdapter: PetAdapter

    private var profile: String = ""
    private var background: String = ""
    private val viewModel: MainViewModel by activityViewModels()

    override fun observeViewModels() {
        lifecycleScope.launch {
            viewModel.pets.collectLatest {
                binding.rcvProfileEditPetProfiles.run{
                    adapter = petAdapter.apply {
                        updateList(it)
                    }
                    layoutManager = LinearLayoutManager(requireContext())
                }
            }
        }
    }

    override fun bindView() {
        with(binding) {
            tvProfileEditName.text = viewModel.currentCustomer.value?.data?.name
            ivProfileEditProfile.setOnClickListener(this@ProfileEditFragment)
            ivProfileEditBackground.setOnClickListener(this@ProfileEditFragment)
            btnProfileEditSave.setOnClickListener(this@ProfileEditFragment)
            tvProfileEditPetProfiles.setOnClickListener(this@ProfileEditFragment)
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                ivProfileEditProfile -> {
                    profileActivityResultLauncher.launch("image/*")
                }

                ivProfileEditBackground -> {
                    backgroundActivityResultLauncher.launch("image/*")
                }

                btnProfileEditSave -> {
                    viewModel.completeProfile(
                        edtProfileEditDateOfBirth.text.toString(),
                        edtProfileEditAddress.text.toString(),
                        edtProfileEditDescribe.text.toString(),
                        profile,
                        background,
                        viewModel.pets.value
                    )
                }
                tvProfileEditPetProfiles -> {
                    findNavController().navigate(ProfileEditFragmentDirections.actionProfileFragmentToPetProfileFragment())
                }
            }
        }
    }

    private var profileActivityResultLauncher = registerForActivityResult<String, Uri>(
        ActivityResultContracts.GetContent()
    ) { result ->
        if (result != null) {
            profile = result.toString()
            binding.civProfileEditProfile.setImageURI(result)
        }
    }

    private var backgroundActivityResultLauncher = registerForActivityResult<String, Uri>(
        ActivityResultContracts.GetContent()
    ) { result ->
        if (result != null) {
            background = result.toString()
            binding.rivProfileEditBackground.setImageURI(result)
        }
    }
}