package com.huongdt315.pethouse.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Customer
import com.huongdt315.pethouse.data.model.Pet
import com.huongdt315.pethouse.data.repo.CustomerRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val customerRepository: CustomerRepository,
) : ViewModel() {


    private var _currentCustomer = MutableStateFlow<ResponseStatus<Customer>?>(null)
    val currentCustomer get() = _currentCustomer.asStateFlow()

    private var _pets = MutableStateFlow<MutableList<Pet>>(mutableListOf())
    val pets get() = _pets.asStateFlow()


    fun getCurrentUser() {
        viewModelScope.launch {
            customerRepository.getCurrentCustomer().collect { response ->
                _currentCustomer.value = response
            }
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch {
            _currentCustomer.update {
                customerRepository.getCustomerByEmailAndPassword(email, password)
            }
        }
    }

    fun registerWithNameEmailPassword(
        name: String,
        email: String,
        password: String
    ) {
        viewModelScope.launch {
            _currentCustomer.update {
                customerRepository.insertCustomer(name, email, password)
            }
        }
    }

    fun completeProfile(
        dateOfBirth: String,
        address: String,
        describe: String,
        profile: String,
        background: String,
        pets: List<Pet>
    ) {
        viewModelScope.launch {
            customerRepository.updateCustomerInformation(
                dateOfBirth,
                address,
                describe,
                profile,
                background,
                pets
            )
        }
    }

    fun addPet(
        name: String,
        profile: String,
        species: String,
        dateOfBirth: String,
        gender: String,
        weight: Double,
        desc: String,
        allergy: String,
        deworming: String,
        vaccination: String
    ) {
        _pets.value.add(
            Pet(
                name = name,
                profile = profile,
                species = species,
                dateOfBirth = dateOfBirth,
                gender = gender,
                weight = weight,
                desc = desc,
                allergy = allergy,
                deworming = deworming,
                vaccination = vaccination
            )
        )
    }

}