package com.huongdt315.pethouse.data.repo

import com.huongdt315.pethouse.data.model.PetCenter
import com.huongdt315.pethouse.data.model.Product
import com.huongdt315.pethouse.data.model.Service
import kotlinx.coroutines.flow.Flow

interface PetCenterRepository {

    suspend fun getPetCenters(): Flow<List<PetCenter>>
    suspend fun getPetCentersInformation(): Flow<List<PetCenter>>
    suspend fun getProductsByPetCenterId(petCenterId: String): Flow<List<Product>>
    suspend fun getServicesByPetCenterId(petCenterId: String): Flow<List<Service>>

    suspend fun getPetCenterById(petCenterId: String): Flow<PetCenter?>

    suspend fun getServiceById(petCenterId: String, serviceId: String): Service?


}