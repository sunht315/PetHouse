package com.huongdt315.pethouse.presentation.component.profile

import android.net.Uri
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.huongdt315.pethouse.databinding.FragmentPetProfileBinding
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.base.BaseFragment

class PetProfileFragment: BaseFragment<FragmentPetProfileBinding>(FragmentPetProfileBinding::inflate) {


    private var profile: String = ""
    private val viewModel: MainViewModel by activityViewModels()

    override fun bindView() {
        with(binding) {
            civProfileEditProfile.setOnClickListener(this@PetProfileFragment)
            btnPetProfileSave.setOnClickListener(this@PetProfileFragment)
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when(v) {
                civProfileEditProfile -> {
                    profileActivityResultLauncher.launch("image/*")
                }
                btnPetProfileSave -> {
                    viewModel.addPet(
                        edtPetProfileName.text.toString(),
                        profile,
                        edtPetProfileSpecies.text.toString(),
                        edtPetProfileDateOfBirth.text.toString(),
                        "Male",
                        edtPetProfileWeight.text.toString().toDouble(),
                        edtPetProfileDescribe.text.toString(),
                        edtPetProfileAllergy.text.toString(),
                        edtPetProfileDewormDate.text.toString(),
                        edtPetProfileVaccinationDate.text.toString(),
                    )
                    findNavController().navigateUp()
                }

                else -> {}
            }
        }
    }

    private var profileActivityResultLauncher = registerForActivityResult<String, Uri>(
        ActivityResultContracts.GetContent()
    ) { result ->
        if (result != null) {
            profile = result.toString()
            binding.civProfileEditProfile.setImageURI(result)
        }
    }

}