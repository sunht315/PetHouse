package com.huongdt315.pethouse.presentation.component.profile

import androidx.navigation.fragment.findNavController
import com.huongdt315.pethouse.databinding.FragmentProfileEditInstructionBinding
import com.huongdt315.pethouse.presentation.base.BaseFragment

class ProfileEditInstructionFragment :
    BaseFragment<FragmentProfileEditInstructionBinding>(FragmentProfileEditInstructionBinding::inflate) {

    override fun bindView() {
        binding.btnProfileEditGetStarted.setOnClickListener {
            findNavController().navigate(
                ProfileEditInstructionFragmentDirections.actionProfileEditInstructionFragmentToProfileFragment()
            )
        }
    }
}