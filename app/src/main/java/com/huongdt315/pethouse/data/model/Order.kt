package com.huongdt315.pethouse.data.model

import java.util.*

data class Order(
    val id: String = UUID.randomUUID().toString(),
    val date: String,
    val status: String = "In process",
    val products: MutableList<ProductCart> = mutableListOf()
)