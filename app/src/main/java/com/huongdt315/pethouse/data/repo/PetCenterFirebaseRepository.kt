package com.huongdt315.pethouse.data.repo

import android.util.Log
import com.google.firebase.FirebaseException
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.huongdt315.pethouse.data.model.PetCenter
import com.huongdt315.pethouse.data.model.Product
import com.huongdt315.pethouse.data.model.Service
import com.huongdt315.pethouse.di.PetCenterDatabaseReferences
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

fun <T> List<T>.asFlowList(): Flow<List<T>> = flow {
    emit(this@asFlowList)
}


@OptIn(ExperimentalCoroutinesApi::class)
class PetCenterFirebaseRepository @Inject constructor(
    @PetCenterDatabaseReferences private val petCenterDatabaseReferences: DatabaseReference
) : PetCenterRepository {
    @OptIn(FlowPreview::class)
    override suspend fun getPetCenters(): Flow<List<PetCenter>> =
        getPetCentersInformation()
            .flatMapConcat { petCenters ->
                petCenters.map { petCenter ->
                    var services = getServicesByPetCenterId(petCenter.id).toList().flatten()
                    petCenter.apply { services = services as MutableList<Service> }
                }.asFlowList()
            }

    override suspend fun getPetCentersInformation(): Flow<List<PetCenter>> {

        val petCenters = mutableListOf<PetCenter>()

        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    petCenters.clear()
                    for (petCenter in snapshot.children) {
                        petCenter.getValue(PetCenter::class.java)
                            ?.let {
                                petCenters.add(it)
                            }
                    }
                    trySend(petCenters)
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }

            petCenterDatabaseReferences.addValueEventListener(valueEventListener)
            awaitClose { petCenterDatabaseReferences.removeEventListener(valueEventListener) }
        }

    }

    override suspend fun getProductsByPetCenterId(petCenterId: String): Flow<List<Product>> {
        val services = mutableListOf<Product>()

        val servicesReferences = petCenterDatabaseReferences.child("$petCenterId/products")

        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    services.clear()
                    for (service in snapshot.children) {
                        service.getValue(Product::class.java)
                            ?.let { services.add(it) }
                    }
                    trySend(services)
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }

            servicesReferences.addValueEventListener(valueEventListener)
            awaitClose { servicesReferences.removeEventListener(valueEventListener) }
        }
    }

    override suspend fun getServicesByPetCenterId(petCenterId: String): Flow<List<Service>> {
        val services = mutableListOf<Service>()

        val servicesReferences = petCenterDatabaseReferences.child("$petCenterId/services")

        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    services.clear()
                    for (service in snapshot.children) {
                        service.getValue(Service::class.java)
                            ?.let { services.add(it) }
                    }
                    trySend(services)
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }

            servicesReferences.addValueEventListener(valueEventListener)
            awaitClose { servicesReferences.removeEventListener(valueEventListener) }
        }
    }

    override suspend fun getPetCenterById(petCenterId: String): Flow<PetCenter?> = callbackFlow {

        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val petCenter = dataSnapshot.getValue(PetCenter::class.java)
                trySend(
                    petCenter
                )
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }

        petCenterDatabaseReferences.child(petCenterId).addValueEventListener(listener)

        awaitClose {
            petCenterDatabaseReferences.child(petCenterId).removeEventListener(listener)
        }
    }

    override suspend fun getServiceById(petCenterId: String, serviceId: String): Service? {
        return try {

            val customer =
                petCenterDatabaseReferences.child("$petCenterId/services/$serviceId").get()
                    .await().getValue(Service::class.java)

            customer?.let { customer }
        } catch (exception: FirebaseException) {
            null
        }
    }
}