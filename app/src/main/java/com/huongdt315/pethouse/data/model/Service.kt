package com.huongdt315.pethouse.data.model

import java.util.UUID

data class Service(
    val id: String = UUID.randomUUID().toString(),
    val name: String = "",
    val image: String = "",
    val desc: String = "",
    val price: Double = 0.0,
    val rating: Double = 0.0
)
