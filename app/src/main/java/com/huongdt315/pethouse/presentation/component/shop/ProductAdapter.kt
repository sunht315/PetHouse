package com.huongdt315.pethouse.presentation.component.shop

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huongdt315.pethouse.data.model.Product
import com.huongdt315.pethouse.databinding.ItemProductBinding
import com.huongdt315.pethouse.presentation.base.BaseAdapter
import java.text.DecimalFormat


class ProductAdapter: BaseAdapter<ItemProductBinding, Product>() {
    override fun bindViewHolder(binding: ItemProductBinding, item: Product) {
        with(binding) {
            Glide.with(root)
                .load(item.image)
                .centerCrop()
                .into(rivItemProductImage)

            tvItemProductName.text = item.name
            tvItemProductType.text = item.type
            tvItemPetCenterSearchRate.text = item.rating.toString()
            rbItemPetCenterSearchStar.rating = item.rating.toFloat()
            tvItemProductPrice.text = "${DecimalFormat("#,###").format(item.price)}đ"
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemProductBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemProductBinding.inflate(layoutInflater, viewGroup, false)
        }
}