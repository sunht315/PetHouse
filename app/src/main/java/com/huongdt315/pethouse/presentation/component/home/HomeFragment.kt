package com.huongdt315.pethouse.presentation.component.home

import android.util.Log
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.huongdt315.pethouse.data.ResponseStatus
import com.huongdt315.pethouse.data.model.Promotion
import com.huongdt315.pethouse.presentation.MainActivity
import com.huongdt315.pethouse.databinding.FragmentHomeBinding
import com.huongdt315.pethouse.presentation.MainViewModel
import com.huongdt315.pethouse.presentation.adapter.PetCenterAdapter
import com.huongdt315.pethouse.presentation.adapter.PromotionAdapter
import com.huongdt315.pethouse.presentation.adapter.ServiceAdapter
import com.huongdt315.pethouse.presentation.adapter.ServiceHomeAdapter
import com.huongdt315.pethouse.presentation.base.BaseFragment
import com.huongdt315.pethouse.presentation.component.PetCenterViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.lang.Error
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    private val petCenterViewModel: PetCenterViewModel by activityViewModels()
    private val mainViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var petCenterAdapter: PetCenterAdapter

    @Inject
    lateinit var promotionAdapter: PromotionAdapter

    @Inject
    lateinit var serviceAdapter: ServiceHomeAdapter

    override fun bindView() {
        petCenterViewModel.getPetCenters()
        promotionAdapter.apply {
            updateList(
                listOf(
                    Promotion(
                        promotion = "https://img.freepik.com/premium-psd/pet-shop-promotion-social-media-instagram-post-banner-template_159024-208.jpg"
                    ),
                    Promotion(
                        promotion = "https://img.freepik.com/premium-psd/pet-shop-promotion-social-media-instagram-story-banner-template_159024-195.jpg"
                    ),
                    Promotion(
                        promotion = "https://s.tmimgcdn.com/scr/800x500/286200/pet-store-promotion-web-banner-template_286259-original.jpg"
                    ),
                    Promotion(
                        promotion = "https://s.tmimgcdn.com/scr/800x500/286200/pet-store-promotion-social-media-post-template_286257-original.jpg"
                    ),
                )
            )
            setOnItemClickListener {
            }
        }

        binding.rcvHomePromotion.run {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = promotionAdapter
        }
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            launch {
                mainViewModel.currentCustomer.collectLatest {
                    when (it) {
                        is ResponseStatus.Success -> {

                        }

                        is ResponseStatus.Error -> {}

                        is ResponseStatus.Loading -> {}

                        else -> {
                            findNavController().navigate(HomeFragmentDirections.actionGlobalLoginFragment())
                        }
                    }
                }
            }
            launch {
                petCenterViewModel.petCenters.collect {
                    petCenterAdapter.apply {
                        updateList(it)
                        setOnItemClickListener {
                            findNavController().navigate(
                                HomeFragmentDirections.actionHomeFragmentToPetCenterFragment(
                                    it.id
                                )
                            )
                        }
                    }

                    binding.rcvHomePetCenter.run {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = petCenterAdapter
                    }
                }
            }
            launch {
                petCenterViewModel.services.collectLatest {
                    serviceAdapter.apply {
                        if (it.isNotEmpty()) {
                            updateList(it)
                            Log.d("ducna", "observeViewModels: $it")
                            setOnItemClickListener {
                                findNavController().navigate(
                                    HomeFragmentDirections.actionHomeFragmentToServiceDetailFragment(
                                        "",
                                        it.id
                                    )
                                )
                            }
                        }
                    }
                    binding.rcvHomeService.run {
                        layoutManager =
                            LinearLayoutManager(
                                requireContext(),
                                LinearLayoutManager.HORIZONTAL,
                                false
                            )
                        adapter = serviceAdapter
                    }
                }
            }
        }
    }

}