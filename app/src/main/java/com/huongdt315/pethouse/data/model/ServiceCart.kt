package com.huongdt315.pethouse.data.model

import java.util.*

data class ServiceCart(
    val id: String = UUID.randomUUID().toString(),
    val service: Service,
    val quantity: Int
)